(function() {
    angular
        .module("toDoListApp")
        .controller("NewCategoryCtrl", NewCategoryCtrl);

    NewCategoryCtrl.$inject = ["$scope", "TaskService","todoSharedService"];

    function NewCategoryCtrl($scope, TaskService, todoSharedService) {
        console.log("Init NewCategoryCtrl");
        var newCategoryCtrl = this;

        newCategoryCtrl.newCategory = "";
        newCategoryCtrl.showForm = false;


        newCategoryCtrl.submitNewCategory = function () {
            console.log("New category is %s", newCategoryCtrl.newCategory);

            TaskService.insertCategory(newCategoryCtrl.newCategory)
            .then(function () {
                console.log("Category added!!");
                todoSharedService.broadcastNewCategory();
            }).catch(function () {
                console.log("Category exists!!");
                alert("Category exists!!");
            }).finally(function () {
                newCategoryCtrl.showForm = false;
                newCategoryCtrl.newCategory = "";
            });
        }

        newCategoryCtrl.closeForm = function () {
            newCategoryCtrl.showForm = false;
        }

        // react to boardcast
        $scope.$on('newCategoryFrmReq', function () {
            newCategoryCtrl.showForm = true;
        })
    }
})();