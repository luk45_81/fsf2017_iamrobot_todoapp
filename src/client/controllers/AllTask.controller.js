(function() {
    angular
        .module("toDoListApp")
        .controller("AllTaskCtrl", AllTaskCtrl);

    AllTaskCtrl.$inject = ["$scope", "TaskService","todoSharedService"];

    function AllTaskCtrl($scope, TaskService, todoSharedService) {
        var allTaskCtrl = this;

        allTaskCtrl.today = new Date();
        allTaskCtrl.today.setHours(0,0,0,0);

        allTaskCtrl.taskList = [];
        allTaskCtrl.selectedCategory = "";
        allTaskCtrl.selectedTaskCategory = "";

        allTaskCtrl.updateTaskList = function() {
            TaskService.retrieveTask()
                .then(function (result) {
                    allTaskCtrl.taskList = result;
                    for (var i = 0; i < allTaskCtrl.taskList.length; i++) {
                        allTaskCtrl.taskList[i].duedate = new Date(allTaskCtrl.taskList[i].duedate);
                        allTaskCtrl.taskList[i].read = "read more";
                        allTaskCtrl.taskList[i].readMore = false;
                        allTaskCtrl.taskList[i].confirmComplete = false;
                    }
                });
        }
        allTaskCtrl.updateTaskList();

        allTaskCtrl.taskOverDue = function(task_status, duedate) {
            return ((task_status == 0) && (duedate < allTaskCtrl.today));
        }

        allTaskCtrl.handleReadMoreLess = function (task_id) {
            var idx = allTaskCtrl.taskList.findIndex(function (elem) {
                return elem.task_id == task_id;
            });
            allTaskCtrl.taskList[idx].readMore = !allTaskCtrl.taskList[idx].readMore;
            allTaskCtrl.taskList[idx].read = (allTaskCtrl.taskList[idx].readMore) ? "read less" : "read more";
        }

        allTaskCtrl.handleEditTask = function (task_id) {
            var idx = allTaskCtrl.taskList.findIndex(function (elem) {
                return elem.task_id == task_id;
            });
            if (idx != -1) {
                todoSharedService.broadcastEditTask(allTaskCtrl.taskList[idx]);
            }        
        };

        allTaskCtrl.handleRemoveTask = function (task_id) {
            var idx = allTaskCtrl.taskList.findIndex(function (elem) {
                return elem.task_id == task_id;
            });
            if (idx != -1) {
                TaskService.removeTask(task_id)
                    .then(function (result) {
                        var removedTask = allTaskCtrl.taskList.splice(idx, 1);
                        todoSharedService.broadcastDeleteTask(removedTask[0]);
                    });
            }
        }

        //NEW COMPLETE TASK
        allTaskCtrl.showConfirmComplete = function(task_id, state) {
            var idx = allTaskCtrl.taskList.findIndex(function (elem) {
                return elem.task_id == task_id;
            });
            // console.log("Index : %s ", task_id);
            allTaskCtrl.taskList[idx].confirmComplete = state;
        }

        allTaskCtrl.handleCompleteTask = function(task_id) {
            var idx = allTaskCtrl.taskList.findIndex(function (elem) {
                return elem.task_id == task_id;
            });
            if (idx != -1) {
                TaskService.completeTask(task_id)
                    .then(function(response) {
                        todoSharedService.broadcastUpdateTaskDetail();
                        console.log("item successfully completed" );
                    });        
            }
            allTaskCtrl.showConfirmComplete(task_id, false);
        }


        $scope.$on('newTaskAdded', function (event, newTask) {
            allTaskCtrl.updateTaskList();
        });

        $scope.$on('deleteTodayTask', function () {
            allTaskCtrl.updateTaskList();
        });

        $scope.$on('deleteTomorrowTask', function () {
            allTaskCtrl.updateTaskList();
        });

        $scope.$on('updateTaskDetail', function () {
            allTaskCtrl.updateTaskList();
        });

        $scope.$on('handleSelCategoryBroadcast', function (event, selCategory) {
            console.log(selCategory);
            allTaskCtrl.selectedCategory = selCategory;
        });

         $scope.$on('handleSelTaskCategoryBroadcast', function (event,selTaskCategory) {
            console.log(selTaskCategory);
            allTaskCtrl.selectedTaskCategory = selTaskCategory.filterValue;
        });
    }
})();