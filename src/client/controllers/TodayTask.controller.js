(function() {
    angular
        .module("toDoListApp")
        .controller("TodayTaskCtrl", TodayTaskCtrl);

    TodayTaskCtrl.$inject = ["$scope", "TaskService","todoSharedService"];

    function TodayTaskCtrl($scope, TaskService, todoSharedService) {
        var todayTaskCtrl = this;

        todayTaskCtrl.taskList = [];
        todayTaskCtrl.today = new Date();
        todayTaskCtrl.today.setHours(0,0,0,0);

        todayTaskCtrl.selectedCategory = "";
        todayTaskCtrl.selectedTaskCategory = "";

        todayTaskCtrl.updateTaskList = function() {
            TaskService.retrieveTask(todayTaskCtrl.today)
                .then(function(result) {
                    todayTaskCtrl.taskList = result;
                    for (var i = 0; i < todayTaskCtrl.taskList.length; i++) {
                        todayTaskCtrl.taskList[i].duedate = new Date(todayTaskCtrl.taskList[i].duedate);
                        todayTaskCtrl.taskList[i].read = "read more";
                        todayTaskCtrl.taskList[i].readMore = false;
                        todayTaskCtrl.taskList[i].confirmComplete = false;
                    }
                });
        }
        todayTaskCtrl.updateTaskList();

        todayTaskCtrl.handleReadMoreLess = function (task_id) {
            var idx = todayTaskCtrl.taskList.findIndex(function (elem) {
                return elem.task_id == task_id;
            });
            todayTaskCtrl.taskList[idx].readMore = !todayTaskCtrl.taskList[idx].readMore;
            todayTaskCtrl.taskList[idx].read = (todayTaskCtrl.taskList[idx].readMore) ? "read less" : "read more";
        }

        todayTaskCtrl.handleEditTask = function (task_id) {
            var idx = todayTaskCtrl.taskList.findIndex(function (elem) {
                return elem.task_id == task_id;
            });
            if (idx != -1) {
                todoSharedService.broadcastEditTask(todayTaskCtrl.taskList[idx]);
            }        
        };

        todayTaskCtrl.handleRemoveTask = function (task_id) {
            var idx = todayTaskCtrl.taskList.findIndex(function (elem) {
                return elem.task_id == task_id;
            });
            if (idx != -1) {
                TaskService.removeTask(task_id)
                    .then(function (result) {
                        todayTaskCtrl.taskList.splice(idx, 1);
                        todoSharedService.broadcastDeleteTodayTask();
                    });
            }
        };

        //NEW COMPLETE TASK
        todayTaskCtrl.showConfirmComplete = function(task_id, state) {
            var idx = todayTaskCtrl.taskList.findIndex(function (elem) {
                return elem.task_id == task_id;
            });
            console.log("Index : %s ", task_id);
            todayTaskCtrl.taskList[idx].confirmComplete = state;
        }

        todayTaskCtrl.handleCompleteTask = function(task_id) {
            var idx = todayTaskCtrl.taskList.findIndex(function (elem) {
                return elem.task_id == task_id;
            });
            if (idx != -1) {
                TaskService.completeTask(task_id)
                    .then(function(response) {
                        todoSharedService.broadcastUpdateTaskDetail();
                        console.log("item successfully completed" );
                    }); 
            }
            todayTaskCtrl.showConfirmComplete(task_id, false);
        };

        $scope.$on('newTaskAdded', function (event, newTask) {
            if (todayTaskCtrl.today.getTime() == newTask.duedate.getTime()) {
                console.log("Got A NEW Task for today >> ");
                todayTaskCtrl.updateTaskList();
            }
        });

        $scope.$on('deleteTask', function (event, removedTask) {
            console.log(removedTask.duedate);
            if (todayTaskCtrl.today.getTime() == removedTask.duedate.getTime()) {
                todayTaskCtrl.updateTaskList();
            }
        });

        $scope.$on('updateTaskDetail', function () {
            todayTaskCtrl.updateTaskList();            
        });

        $scope.$on('handleSelCategoryBroadcast', function (event, selCategory) {
            console.log(selCategory);
            todayTaskCtrl.selectedCategory = selCategory;
        });

        $scope.$on('handleSelTaskCategoryBroadcast', function (event,selTaskCategory) {
            console.log(selTaskCategory);
            todayTaskCtrl.selectedTaskCategory = selTaskCategory.filterValue;
        });
    }
})();