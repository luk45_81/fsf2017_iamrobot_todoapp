(function() {
    // Attaches DeptService service to the DMS module
    angular
        .module("toDoListApp")
        .service("TaskService", TaskService);

    TaskService.$inject = ['$http'];

    function TaskService($http) {
        var service = this;

        // Exposed functions -------------------------------------------------------------------------------------------
        service.retrieveTask = retrieveTask;
        service.insertTask = insertTask;
        service.updateTask = updateTask;
        service.removeTask = removeTask;
        service.completeTask = completeTask;

        service.retrieveCategory = retrieveCategory;
        service.insertCategory = insertCategory;


        // Function declaration and definition -------------------------------------------------------------------------
        function retrieveTask(dateRequired) {
            if (dateRequired) {
                return $http({
                    method: 'GET',
                    url: '/tasks',
                    params: {
                        duedate:dateRequired
                    }
                }).then(function (result) {
                    return result.data;
                });
            } else {
                return $http({
                    method: 'GET',
                    url: '/tasks'
                }).then(function (result) {
                    return result.data;
                });         
            }
            return null;
        }

        function insertTask(newTaskDetail) {
            return $http({
                    method: 'POST',
                    url: '/tasks',
                    data: newTaskDetail
                }).then(function (result) {
                    //console.log(result.data);
                    return result.data;
                });        
        }

        function updateTask(newTaskDetail) {
            return $http({
                    method: 'PUT',
                    url: '/tasks/' +  newTaskDetail.task_id,
                    data: newTaskDetail
                }).then(function (result) {
                    //console.log(result.data);
                    return result.data;
                });        
        }

        function removeTask(task_id) {
            return $http({
                method: 'DELETE',
                url: '/tasks/' +  task_id,
            });
        } 

        function completeTask(task_id) {
            return $http({
                    method: 'PUT',
                    url: '/tasks/' +  task_id,
                    data: {task_status:1}
                }).then(function (result) {
                    //console.log(result.data);
                    return result.data;
                });        
        }

        function retrieveCategory() {
            return $http({
                    method: 'GET',
                    url: "/categories"
                }).then(function (result) {
                    return result.data;
                });
        }

        function insertCategory(newCategory) {
            return $http({
                    method: 'POST',
                    url: '/categories',
                    data: { 
                        cat_name: newCategory 
                    }
            }).then(function (result) {
                    return result.data;
            });
        }

    }
})();