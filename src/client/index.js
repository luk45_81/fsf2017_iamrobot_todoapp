(function () {
    var toDoListApp = angular.module("toDoListApp", ['ngMaterial']);

    var SelectionCtrl = function($scope) {
        var selectionCtrl = this;

        selectionCtrl.selectedTaskStatus = "";
        selectionCtrl.selectedCategory = "";

        $scope.$on('handleSelCategoryBroadcast', function (event, selCategory) {
            if (selCategory != "") {
                selectionCtrl.selectedCategory = " :: " + selCategory;
            } else {
                selectionCtrl.selectedCategory = "";
            }
        });

        $scope.$on('handleSelTaskCategoryBroadcast', function (event,selTaskCategory) {
            if (selTaskCategory.filterValue != "") {
                selectionCtrl.selectedTaskStatus = " :: " + selTaskCategory.catName;
            } else {
                selectionCtrl.selectedTaskStatus = "";
            }
        });
    }


    // Start of Controller Functions
    var TopMenuCtrl = function (todoSharedService) {
        var toMenuCtrl = this;

        toMenuCtrl.newCategoryFrmReq = function () {
            console.log("new category form request");
            todoSharedService.broadcastNewCategoryFrmReq();
        }

        toMenuCtrl.newTaskFrmReq = function () {
            console.log("new task form request");
            todoSharedService.broadcastNewTaskFrmReq();
        }
    }

    var CategoryCtrl = function ($scope, TaskService, todoSharedService) {

        var categoryCtrl = this;

        categoryCtrl.updateCategory = function () {
            TaskService.retrieveCategory()
                .then(function (result) {
                    categoryCtrl.categorizedTasks = result;
                });
        }

        categoryCtrl.newCategory = "";
        categoryCtrl.categorizedTasks = [];

        categoryCtrl.allTasks = [   {catName: "All Tasks",                                    
                                    filterValue:""},

                                    {catName: "Completed Tasks",                                    
                                    filterValue:"1"},

                                    {catName: "Pending Tasks",                                    
                                    filterValue:"0"} ];

        categoryCtrl.updateCategory();

        categoryCtrl.selectCategory = function(selectedCategory) {
            todoSharedService.broadcastSelCategory(selectedCategory);
        }

        categoryCtrl.selectTaskCategory = function(filterValue) {
            todoSharedService.broadcastSelTaskCategory(filterValue);
        }

        $scope.$on('handleCatBroadcast', function () {
            categoryCtrl.updateCategory();
        })

    };

    // End of Controller Functions
    toDoListApp.controller("SelectionCtrl", ["$scope", SelectionCtrl]);
    toDoListApp.controller("TopMenuCtrl", ["todoSharedService", TopMenuCtrl]);
    toDoListApp.controller("CategoryCtrl", ["$scope", "TaskService", "todoSharedService", CategoryCtrl]);
})(); 