module.exports = function(connection, Sequelize) {
    var TaskCat = connection.define(
        "taskcat", {
            task_id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                allowNull: false,
                references: {
                    model: 'tasks',
                    key: 'task_id'
                }
            },
            cat_id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                allowNull: false,
                references: {
                    model: 'categories',
                    key: 'cat_id'
                }
            }          
        },{
            tableName: 'taskcats',
            timestamps: false
        }
    );
    return TaskCat;
}