module.exports = function(connection, Sequelize) {
    var Category = connection.define(
        "category", {
            cat_id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false
            },
            cat_name: {
                type: Sequelize.STRING,
                uniqueKey: true,
                allowNull: false
            }
        },{
            tableName: 'categories',
            timestamps: false
        }
    );
    return Category;
}