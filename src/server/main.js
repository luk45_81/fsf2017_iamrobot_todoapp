// Load the Libraries
var path = require("path");
var express = require("express");
var bodyParser = require("body-parser");
var todoAppEngine = require("./todoAppEngine.js");

// Create an Instance of the app
var app = express();

var todoDB = require("./database");

var myTaskEngine = todoAppEngine();

// For Testing only, create some dummy task
myTaskEngine.initDummyRecord();

// End of dummy data

// Define the Port that the app listen to
app.set("port", parseInt(process.argv[2]) || parseInt(process.env.APP_PORT) || 3000);

const CLIENT_FOLDER = path.join(__dirname, "../client");
const ASSETS_FOLDER = path.join(CLIENT_FOLDER, "assets");
const IMAGES_FOLDER = path.join(CLIENT_FOLDER, "images");
const LIBS_FOLDER  = path.join(CLIENT_FOLDER, "bower_components");

// Define Routes to load the contents
app.use(express.static(CLIENT_FOLDER));
app.use(express.static(IMAGES_FOLDER));
app.use("/assets", express.static(ASSETS_FOLDER));
app.use("/libs", express.static(LIBS_FOLDER));

app.use(bodyParser.json());
//app.use(bodyParser.urlencoded({extended: false})); // not to use the default, to only accept JSON

// Other routes
require("./database.route")(app, todoDB);

// Scheduler to check tomorrow's due and overdue tasks
require("./scheduler")(todoDB);


// Start the Server application
app.listen(app.get("port"), function() {
    console.log("Application started at %s on %d", new Date(), app.get("port"));
});