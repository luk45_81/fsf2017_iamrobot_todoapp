var Sequelize = require('sequelize');
var config = require("./config");
var database;

database = new Sequelize(
    config.mysql.database,
    config.mysql.username,
    config.mysql.password, {
        host: config.mysql.host,
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        }
    });


var TaskModel = require('./models/task.model')(database, Sequelize);
var TaskCategoryModel = require('./models/taskcat.model')(database, Sequelize);
var CategoryModel = require('./models/category.model')(database, Sequelize);

// BEGIN: MYSQL RELATIONS

TaskModel.hasMany(TaskCategoryModel, {
    foreignKey: 'task_id'
});

TaskCategoryModel.belongsTo(CategoryModel, {
    foreignKey: 'cat_id', targetKey: 'cat_id'
});

// END: MYSQL RELATIONS

module.exports = {
    Task: TaskModel,
    TaskCategory: TaskCategoryModel,
    Category: CategoryModel
};


database.sync({ force: true }).
    then(function() {
        initDummyData();
    }).catch(function(error) {
        console.log(error);
    });


function initDummyData() {
    var today = new Date();
    today.setHours(0,0,0,0);
    console.log("Today: " + today);

    var yesterday = new Date();
    yesterday.setHours(0,0,0,0);
    yesterday.setDate(yesterday.getDate() - 1);
    console.log("Yesterday: " + yesterday);

    var tomorrow = new Date();
    tomorrow.setHours(0,0,0,0);
    tomorrow.setDate(tomorrow.getDate() + 1);
    console.log("Tomorrow: " + tomorrow);

    TaskModel.bulkCreate([
            {duedate: yesterday , task_status:0, task_title: "NUS-ISS class", task_desc: "Now use Lorem Ipsum as their default model text, and a search"},
            {duedate: today     , task_status:0, task_title: "Search@#$ function TEST", task_desc: "It is a long act that a reader will be distracted by the readable content of a page when looking at its layout."},
            {duedate: today     , task_status:1, task_title: "Family dinner", task_desc: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout."},
            {duedate: today     , task_status:0, task_title: "Meeting @ work", task_desc: "Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search"},
            {duedate: today     , task_status:1, task_title: "Project agile", task_desc: "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin years old."},
            {duedate: tomorrow  , task_status:0, task_title: "Kids tuition $$", task_desc: "Random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old."},
            {duedate: tomorrow  , task_status:0, task_title: "Appointment ##", task_desc: "Random text. It has rclassical Latin literature from 45 BC, Lorem Ipsum is not simext. Making it over 2000 years old."},
            {duedate: yesterday , task_status:0, task_title: "Family outing", task_desc: "Random text. It has roots in a piece of Ipsum is not simply random text. Making it over 2000 years old."},
            {duedate: today     , task_status:0, task_title: "** Boss meeting!! **", task_desc: "Now use Lorem Ipsum as their default model text, and a search"}
    ]).then(function() {
        console.log("Done Creating Tasks");
        CategoryModel.bulkCreate([
            {cat_name: "Personal"},
            {cat_name: "Family"},
            {cat_name: "Work"}
        ]).then(function() {
            console.log("Done Creating Category");
            TaskCategoryModel.bulkCreate([
                {task_id:1, cat_id:3},
                {task_id:2, cat_id:2},
                {task_id:3, cat_id:2},
                {task_id:4, cat_id:3},
                {task_id:5, cat_id:3},
                {task_id:6, cat_id:2},
                {task_id:7, cat_id:1},
                {task_id:8, cat_id:2},
                {task_id:9, cat_id:3},
            ]).then(function(){
                console.log("--->>> Done Creating Task-Category\n\n");
            });
        });
    });
}

