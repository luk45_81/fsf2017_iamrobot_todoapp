var schedule = require('node-schedule');
var mailer = require("./mailer")();
 
module.exports = function(todoDB){
    console.log("Scheduler is loaded");
    
    var todayDate = null;
    var tomorrowDate = null;
    var mailHeader="";
    var mailText="";

    //schedule.scheduleJob('*/5 * * * * *', function(){   ====> every 5 secs
    //schedule.scheduleJob('0 */5 * * * *', function(){   ====> every 5 mins
    //schedule.scheduleJob('0 0 */5 * * *', function(){   ====> every 5 hours
    //schedule.scheduleJob('0 32 13 * * *', function(){    ====> everyday at 13:32
    schedule.scheduleJob('0 */1 * * * *', function(){
        todayDate = new Date();
        tomorrowDate = new Date();        
        mailHeader="";
        mailText="";
        todayDate.setHours(0,0,0,0);
        tomorrowDate.setHours(0,0,0,0);
        tomorrowDate.setDate(tomorrowDate.getDate()+1);

        console.log("Getting tasks due tomorrow");


        todoDB.Task
            .findAll({
                where: {
                    $and:[
                        {duedate:tomorrowDate},
                        {task_status:0}
                    ]
                }, 
                include: [{
                    model: todoDB.TaskCategory, 
                    limit: 1,
                    include: [{
                        model: todoDB.Category
                    }]
                }]
            }).then(function(tasklist) {
                console.log("Tomorrow Tasks:" + tasklist.length)
                if (tasklist.length>0){
                    mailHeader = "TodoApp Reminder: " +tasklist.length+ " tasks are pending tomorrow";
                    mailText = "<h1>Following are the tasks pending tomorrow:</h1>"
                    tasklist.forEach(function(task,idx){
                        mailText=mailText+"<p> From Category "+ task.taskcats[0].category.cat_name+ " : "+task.task_title+ "</p>";
                    })
                    mailText=mailText+"<p> Report date: "+ todayDate + "</p>";
                    console.log("Mail Body: "+ mailText);
                    mailer.sendMail(mailHeader,mailText);
                }
            }).catch(function(err){
                console.log("Error in getting task due tomorrow:"+err);                
            });
        
        console.log("Getting overdue tasks");

        todoDB.Task
            .findAll({
                where: {
                    $and:[
                        {duedate:{$lt:todayDate}},
                        {task_status:0}
                    ]
                }, 
                include: [{
                    model: todoDB.TaskCategory, 
                    limit: 1,
                    include: [{
                        model: todoDB.Category
                    }]
                }]
            }).then(function(tasklist) {
                console.log("Overdue Tasks:" + tasklist.length)
                if (tasklist.length>0){
                    mailHeader = "TodoApp Alert: You have " +tasklist.length+ " overdue tasks";
                    mailText = "<h1>Following are the overdue tasks:</h1>"
                    tasklist.forEach(function(task,idx){
                        mailText=mailText+"<p> From Category "+ task.taskcats[0].category.cat_name+ " : "
                            +task.task_title + " (Due Date: "+task.duedate.toDateString() +") </p>";
                    });
                    mailText=mailText+"<p> Report date: "+ todayDate + "</p>";
                    console.log("Mail Body: "+ mailText);
                    mailer.sendMail(mailHeader,mailText);
                }
            }).catch(function(err){
                console.log("Error in getting overdue tasks:"+err);                
            });
            
        //mailer.sendMail(mailHeader,mailText);
    });
}


